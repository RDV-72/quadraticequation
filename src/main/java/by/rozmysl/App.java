package by.rozmysl;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        double a = 5;
        double b = -42;
        double c = 7;
        Arrays.stream(Equation.findRoots(a, b, c)).forEach(System.out::println);
    }
}