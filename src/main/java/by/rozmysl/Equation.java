package by.rozmysl;

public class Equation {

    public static double[] findRoots(double a, double b, double c) {
        double[] cases = checkSpecialCases(a, b, c);
        if (cases != null) return cases;
        double discriminant = b * b - 4 * a * c;
        if (discriminant > 0) return
                new double[]{(-b - Math.sqrt(discriminant)) / (2 * a), (-b + Math.sqrt(discriminant)) / (2 * a)};
        if (discriminant == 0) return new double[]{-b / (2 * a)};
        return new double[0];
    }

    public static double[] checkSpecialCases(double a, double b, double c) {
        if (a == 0 && b == 0 && c == 0) return new double[]{Double.POSITIVE_INFINITY};
        if (a == 0 && b == 0) return new double[0];
        if (a == 0) return new double[]{-c / b};
        return null;
    }
}