package by.rozmysl;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class EquationTest {

    @Test
    public void testFindRoots_NO_NULL() {
        Assert.assertNotNull(Equation.findRoots(1, 4, 4));
    }

    @Test
    public void testCheckSpecialCases() {
        Assert.assertArrayEquals(Equation.checkSpecialCases(0, 0, 0),
                new double[]{Double.POSITIVE_INFINITY}, 0.0001);
        Assert.assertArrayEquals(Equation.checkSpecialCases(0, 0, 5), new double[0], 0.0001);
        Assert.assertArrayEquals(Equation.checkSpecialCases(0, 2, 8), new double[]{-4}, 0.0001);
    }

    @Test
    public void testCheckRootsWhenDiscriminantIsGreaterThanZero() {
        Assert.assertArrayEquals(Equation.findRoots(5, -34, -8), new double[]{-0.2277, 7.0277}, 0.0001);
    }

    @Test
    public void testCheckRootsWhenDiscriminantIsZero() {
        Assert.assertArrayEquals(Equation.findRoots(1, -12, 36), new double[]{6.0}, 0.0001);
    }

    @Test
    public void testCheckRootsWhenDiscriminantIsLessThanZero() {
        Assert.assertArrayEquals(Equation.findRoots(5, 4, 7), new double[0], 0.0001);
    }

    @Test
    public void testCheckRootsUsingVietaTheorem() {
        Assert.assertEquals(Arrays.stream(Equation.findRoots(1, -8, 12)).sum(), 8, 0.0001);
        Assert.assertEquals(Arrays.stream(Equation.findRoots(1, -8, 12))
                .reduce(1, (x, y) -> x * y), 12, 0.0001);
    }
}